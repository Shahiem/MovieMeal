$('#searchMultiple').search({
    apiSettings: {
        url: baseUrl + 'ajax/multiple?q={query}'
    },
    fields: {
        results: 'items',
        title: 'name',
        description: 'email',
    },
    minCharacters: 2,
    maxResults: 15,
    error: {
        noResults: 'Er zijn geen zoekresultaten gevonden.',
        serverError: 'Er is een fout opgetreden met het uitvoeren van een query.'
    },
    onSelect: function(response) {
        // $('input[name="user_id"]').val(response.id);
    }
});

$('#usersSearch').search({
    apiSettings: {
        url: baseUrl + 'ajax/users?q={query}'
    },
    fields: {
        results: 'items',
        title: 'name',
        link: 'link',
        description: 'email',
    },
    minCharacters: 2,
    maxResults: 15,
    error: {
        noResults: 'Er zijn geen zoekresultaten gevonden.',
        serverError: 'Er is een fout opgetreden met het uitvoeren van een query.'
    },
    onSelect: function(response) {
        $('input[name="user_id"]').val(response.id);
    }
});

$('#guestsSearch').search({
    apiSettings: {
        url: baseUrl + 'ajax/guests/' + $('input[name="company_id"]').val() + '?q={query}'
    },
    fields: {
        results: 'items',
        title: 'name',
        description: 'email',
    },
    minCharacters: 2,
    maxResults: 15,
    error: {
        noResults: 'Er zijn geen zoekresultaten gevonden.',
        serverError: 'Er is een fout opgetreden met het uitvoeren van een query.'
    },
    onSelect: function(response) {
        $('input[name="name"]').val(response.name);
        $('input[name="email"]').val(response.email);
        $('input[name="phone"]').val(response.phone);
    }
});

$('#usersCompaniesSearch1').search({
    apiSettings: {
        url: baseUrl + 'ajax/companies/users?q={query}'
    },
    fields: {
        results: 'items',
        title: 'name',
        url: 'link'
    },
    minCharacters: 2,
    maxResults: 15,
    error: {
        noResults: 'Er zijn geen zoekresultaten gevonden.',
        serverError: 'Er is een fout opgetreden met het uitvoeren van een query.'
    }
});

$('#companiesSearch').search({
    apiSettings: {
        url: baseUrl + 'ajax/companies?q={query}'
    },
    fields: {
        results: 'items',
        title: 'name',
        url: 'link'
    },
    minCharacters: 2,
    maxResults: 15,
    error: {
        noResults: 'Er zijn geen zoekresultaten gevonden.',
        serverError: 'Er is een fout opgetreden met het uitvoeren van een query.'
    }
});

$('#invoicesSearch').search({
    apiSettings: {
        url: baseUrl + 'ajax/companies/invoices?q={query}'
    },
    fields: {
        results: 'items',
        title: 'name',
        url: 'link'
    },
    minCharacters: 3,
    maxResults: 15,
    error: {
        noResults: 'Er zijn geen zoekresultaten gevonden.',
        serverError: 'Er is een fout opgetreden met het uitvoeren van een query.'
    }
});

$('#companiesOwnersSearch').search({
    apiSettings: {
        url: baseUrl + 'ajax/companies/owners?q={query}'
    },
    fields: {
        results: 'items',
        title: 'name',
        description: 'email'
    },
    minCharacters: 2,
    maxResults: 15,
    error: {
        noResults: 'Er zijn geen zoekresultaten gevonden.',
        serverError: 'Er is een fout opgetreden met het uitvoeren van een query.'
    },
    onSelect: function(response) {
        $('input[name="owner"]').val(response.id);
    }
});

$('#companiesCallerSearch').search({
    apiSettings: {
        url: baseUrl + 'ajax/companies/callers?q={query}'
    },
    fields: {
        results: 'items',
        title: 'name',
        description: 'email'
    },
    minCharacters: 2,
    maxResults: 15,
    error: {
        noResults: 'Er zijn geen zoekresultaten gevonden.',
        serverError: 'Er is een fout opgetreden met het uitvoeren van een query.'
    },
    onSelect: function(response) {
        $('input[name="caller"]').val(response.id);
    }
});

$('#companiesWaitersSearch').search({
    apiSettings: {
        url: baseUrl + 'ajax/companies/waiters?q={query}'
    },
    fields: {
        results: 'items',
        title: 'name',
        description: 'email'
    },
    minCharacters: 2,
    maxResults: 15,
    error: {
        noResults: 'Er zijn geen zoekresultaten gevonden.',
        serverError: 'Er is een fout opgetreden met het uitvoeren van een query.'
    },
    onSelect: function(response) {
        $('input[name="waiter"]').val(response.id);
    }
});

$('#barcodesCompaniesSearch').search({
    apiSettings: {
        url: baseUrl + 'ajax/companies/barcodes?q={query}'
    },
    fields: {
        results: 'items',
        title: 'name',
        url: 'link'
    },
    minCharacters: 2,
    maxResults: 15,
    error: {
        noResults: 'Er zijn geen zoekresultaten gevonden.',
        serverError: 'Er is een fout opgetreden met het uitvoeren van een query.'
    }
});

<!DOCTYPE html>
<html lang="nl">
<head>
    <title>{{ isset($pageTitle) ? $pageTitle : 'Reserveer in enkele stappen met uw spaartegoed!' }} - MovieMeal</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/images/icons/apple-icon-180x180.png') }}">
    <link rel="shortcut icon" href="{{ asset('public/images/icons/favico.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('public/images/icons/favico.ico') }}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('public/css/app.css?rand='.str_random(40)) }}">
    <link href="{{ captcha_layout_stylesheet_url() }}" type="text/css" rel="stylesheet">

    @yield('styles')

    <link rel="shortcut icon" sizes="144x144" href="{{ asset('public/launch_144.png') }}"> 
    <link rel="shortcut icon" sizes="128x128" href="{{ asset('public/launch_129.png') }}"> 
    <meta name="mobile-web-app-capable" content="yes">

    <link rel="manifest" href="{{ asset('public/manifest.json') }}">
    <meta name="robots" content="nofollow" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
    <meta name="description" content="{{ isset($metaDescription) ? strip_tags($metaDescription) : 'Reserveer in enkele stappen met uw spaartegoed!' }}">
</head>
<body <?php echo Route::getCurrentRoute()->uri() == '/' ? 'class="index"' : ''; ?>>
    <div id="searchFull" style="display: none;">
        <?php echo Form::open(array('url' => 'search-redirect', 'class' => 'ui form', 'method' => 'post')) ?>
        <div class="fields">
            <div id="searchInput" class="sixteen wide field">
                <div class="ui company search">
                    <div class="ui black inverted fluid icon input">
                        <input type="text" class="prompt" name="q" placeholder="Welk restaurant zoekt u?">
                        <i class="circular search link icon"></i>
                    </div>
                    <div class="results"></div>
                </div>
            </div>
        </div>
        <?php echo Form::close(); ?>
    </div>

    <div class="ui small modal">
        <i class="close icon"></i>
        <div class="header"></div>
        <div class="content">
        </div>
    </div>

    <div id="pageLoader">
        <div class="content">
            <div class="ui active inverted dimmer">
                <div class="ui text loader">Laden..</div>
            </div>
        </div>
    </div>

    <div id="example1" class="fixMenu">
        <a href="{{ url('/') }}" class="ui red icon big launch right attached fixed button">
            <i class="home icon"></i>
            <span class="text">Menu</span>
        </a><br />

        @if ($__env->yieldContent('fixedMenu'))
             @yield('fixedMenu')
        @else
            <a href="{{ url('faq') }}" class="ui black icon big launch right attached fixed button">
                <i class="question mark icon"></i>
                <span class="text">Veelgestelde vragen</span>
            </a><br />
        @endif

        <a href="{{ url('contact') }}" class="ui yellow icon big launch right attached fixed button">
            <i class="envelope icon"></i>
            <span class="text">Contact</span>
        </a><br>

        <a href="#" class="ui grey icon big launch right attached fixed button search-full-open">
            <i class="search icon"></i>
            <span class="text">Zoeken</span>
        </a><br>

        <a href="https://www.facebook.com/Uwvoordeelpas-321703168185624/?fref=ts" target="_blank" class="ui blue icon big launch right attached fixed button">
            <i class="facebook icon"></i>
            <span class="text">Facebook</span>
        </a>
    </div>


    <div class="ui sidebar black right inverted vertical menu">
        <a href="#" class="close bar item">
            <strong><i class="close icon"></i> Sluit menu</strong>
        </a>

        <div class="item">
            <div class="ui accordion inverted">
                <div class="item title">
                   <i class="{{ Session::has('locale') ? $convertLocale[Session::get('locale')] : 'nl' }} flag"></i> {{ strtoupper(Session::get('locale')) }} <i class=" dropdown icon"></i>
                </div>

                <div class="content">
                    <a href="{{ url('setlang/nl?redirect='.Request::url()) }}" data-value="nl" class="item"><i class="nl flag"></i> NL</a>
                    <a href="{{ url('setlang/en?redirect='.Request::url()) }}" data-value="en" class="item"><i class="gb flag"></i> EN</a>
                    <a href="{{ url('setlang/be?redirect='.Request::url()) }}" data-value="be" class="item"><i class="be flag"></i> BE</a>
                    <a href="{{ url('setlang/de?redirect='.Request::url()) }}" data-value="de" class="item"><i class="de flag"></i> DE</a>
                    <a href="{{ url('setlang/fr?redirect='.Request::url()) }}" data-value="fr" class="item"><i class="fr flag"></i> FR</a>
                </div>
            </div>
        </div>

        <div id="btnSave" class="item"><i class="film icon"></i> Download de app</div>
        <a href="{{ url('cinemas') }}" class="item"><i class="film icon"></i> Bioscopen</a>

        @if ($userCompany OR $userWaiter)
        <a href="{{ url('faq/3/restaurateurs') }}" class="item"><i class="question mark icon"></i> Veelgestelde vragen</a>
        @else
        <a href="{{ url('faq/2/leden') }}" class="item"><i class="question mark icon"></i> Veelgestelde vragen</a>
        @endif
        
        <a href="#" class="item search-full-open"><i class="search icon"></i> Zoeken</a><br>

        @if ($userAuth)        
            @include('template/navigation/company')
            @include('template/navigation/callcenter')
            @include('template/navigation/admin')
        @else
            <a href="{{ url('hoe-werkt-het') }}" class="item"><i class="file text outline icon"></i> Hoe werkt het?</a>
            <a href="{{ url('algemene-voorwaarden') }}" class="item"><i class="book icon"></i> Voorwaarden</a>
        @endif
    </div>

    <div class="pusher">
        @if (!Request::has('iframe'))
            <header>  
                <div class="headerShadow">
                    <div class="container">
                        <div class="header website">
                            <a href="{{ url('/') }}" class="logo">
                                <img src="{{ url('public/images/vplogo.png') }}">
                            </a>

                            <div class="right">
                                <ul>
                                    @if ($userAuth)
                                    <li class="icon link" data-content="Uitloggen">
                                        <a href="{{ url('logout') }}"><i class="sign out icon"></i></a>
                                    </li>
                                    @endif

                                    <li class="icon link">
                                        <a href="#" id="search-open"><i class="search icon"></i></a>                       
                                    </li>

                                    <li class="icon link">
                                        <i class="sidebar open icon"></i>
                                    </li>
                                </ul>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </header>

            <setion>
            @if (isset($__env->getSections()['slider']))
                @yield('slider')
            @else
                @include('template/slider')
            @endif
            </setion>
        @endif

        <section class="content">
             @yield('content')
        </section>

        <footer>
            <div class="footerWrap">
                <div class="footer">
                    <div class="container">
                        <div class="col">
                            {!! isset($contentBlock[4]) ? $contentBlock[4] : '' !!}

                             <div class="ui inverted">
                                <div class=" title">
                                    <h4><i class="dropdown icon"></i> Leden</h4>
                                    <div class="ui divider"></div>
                                </div>

                                <div class="content">
                                    @if(isset($pageLinks[1]))
                                        <ul>
                                            @foreach ($pageLinks[1] as $data)
                                                <li><a href="{{ url($data['slug']) }}" id="{{ (isset($data['link_to']) && $data['link_to'] == 'register' ? 'registerButton4' : '') }}" class="{{ isset($data['link_to']) && $data['link_to'] == 'login' ? 'login button' : '' }}" data-type="login">{{ $data['title'] }}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            {!! isset($contentBlock[5]) ? $contentBlock[5] : '' !!}

                            <div class="ui inverted">
                                <div class=" title">
                                    <h4><i class="dropdown icon"></i> Bedrijven</h4>
                                    <div class="ui divider"></div>
                                </div>

                                <div class="content">
                                    <ul>
                                        @if (isset($pageLinks[2]))
                                            @foreach ($pageLinks[2] as $data)
                                                <li><a href="{{ url($data['slug']) }}" id="{{ (isset($data['link_to']) && $data['link_to'] == 'register' ? 'registerButton4' : '') }}" class="{{ isset($data['link_to']) && $data['link_to'] == 'login' ? 'login button' : '' }}" data-type="login">{{ $data['title'] }}</a></li>
                                            @endforeach
                                        @endif

                                        @if (!$userAuth)
                                        <li><a href="#" id="registerButton">{{ trans('app.register') }}</a></li>
                                        <li><a href="#" class="login" data-type="login">{{ trans('app.login') }}</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            {!! isset($contentBlock[6]) ? $contentBlock[6] : '' !!}

                            <div class="ui inverted">
                                <div class=" title">
                                    <h4><i class="dropdown icon"></i> Algemeen</h4>

                                    <div class="ui divider"></div>
                                </div>

                                <div class="content">
                                    @if(isset($pageLinks[3]))
                                        <ul>
                                            @foreach ($pageLinks[3] as $data)
                                                <li><a href="{{ url($data['slug']) }}" id="{{ (isset($data['link_to']) && $data['link_to'] == 'register' ? 'registerButton4' : '') }}" class="{{ isset($data['link_to']) && $data['link_to'] == 'login' ? 'login button' : '' }}" data-type="login">{{ $data['title'] }}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif

                                    <ul>
                                        <li><a href="{{ url('sitemap.xml') }}" target="_blank" class="item">Sitemap</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            {!! isset($contentBlock[7]) ? $contentBlock[7] : '' !!}

                            <div class="ui inverted">
                                <div class="title">
                                    <h4><i class="dropdown icon"></i> Voorwaarden</h4>

                                    <div class="ui divider"></div>
                                </div>
                                
                                <div class="content">
                                    @if(isset($pageLinks[4]))
                                        <ul>
                                            @foreach ($pageLinks[4] as $data)
                                                <li><a href="{{ url($data['slug']) }}" id="{{ (isset($data['link_to']) && $data['link_to'] == 'register' ? 'registerButton4' : '') }}" class="{{ isset($data['link_to']) && $data['link_to'] == 'login' ? 'login button' : '' }}" data-type="login">{{ $data['title'] }}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <script type="text/javascript" src="{{ asset('public/js/app.js?rand='.str_random(40)) }}"></script>
    
    @if (!Request::has('iframe'))
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjrbVJMJKWzCl8JZWV3_5Jy5P4CTITznU&callback=initMap"></script>
    @endif

    @yield('scripts')
    @include('sweet::alert')
    @include('admin.template.search.js')

    <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-88679351-1', 'auto');
        ga('send', 'pageview');
    </script>  

    <script type="text/javascript">
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register(baseUrl + 'sw.js').then(function(registration) {
         console.log('ServiceWorker registration successful with scope: ', registration.scope);
      }).catch(function(err) {
        console.log('ServiceWorker registration failed: ', err);
      });
    }
    </script>

    <!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
    <script type="text/javascript">
        window.cookieconsent_options = {"message":"Deze website maakt gebruik van cookies.","dismiss":"Ik ga akkoord","learnMore":"Meer informatie","link":"https://www.uwvoordeelpas.nl/disclaimer","theme":"light-floating"};
    </script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js"></script>
    <!-- End Cookie Consent plugin -->
    
    <script type="text/javascript">
    $(document).ready(function() {
        @if(count($errors) > 0)
            var errorMessage = [];
            <?php 
            foreach ($errors->all() as $error) {
            ?>
                errorMessage.push('<li><?php echo $error; ?></li>');
                <?php
            }
            ?>

            swal({ 
                title: "Oeps, er ging iets fout!", 
                html: true, 
                text: '<ul>' + errorMessage.join(' ') + '</ul>', 
                type: "error", 
                confirmButtonText: "OK" 
            });
        @endif

        $("#pageLoader").fadeOut('slow');
    });
    </script>
</body>
</html>
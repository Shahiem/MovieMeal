@if (count($userCompanies) >= 1)
	@foreach ($userCompanies as $company)
		<a href="{{ url('admin/companies/update/'.$company->id.'/'.$company->slug) }}" class="item"><strong>{{ $company->name }}</strong></a>
		<a href="{{ url('account') }}" class="item"><i class="user icon"></i>  Mijn gegevens</a>

		<a href="{{ url('admin/companies/update/'.$company->id.'/'.$company->slug.($company->signature_url == null ? '?step=1' : '')) }}" class="item"><i class="building icon"></i> {{ $company->signature_url == null ? 'Inschrijving' : 'Bedrijfsgegevens' }}</a>
		<a href="{{ url('admin/invoices/overview/'.$company->slug) }}" class="{{ $company->signature_url == null ? 'disabled' : '' }} item"><i class="euro icon"></i> Facturen</a>
		<a href="{{ url('admin/companies/contract/'.$company->id.'/'.$company->slug) }}" target="_blank" class="{{ $company->signature_url == null ? 'disabled' : '' }} item"><i class="file pdf outline icon"></i> Contract</a>
		<a href="{{ url('logout') }}" class="item"><i class="sign out icon"></i> Uitloggen</a><br>
	@endforeach
@endif
<div id="sliderImage" class="slider{{ Request::is('admin/*') == TRUE ? ' admin' : '' }}">
    @if (count($sliderMedia) >= 1)
        @foreach ($sliderMedia as $id => $images)
        <img id="bg-{{ $id }}" class="background {{ $id > 0 ? 'hide' : '' }}" src="{{ url('public/'.$images->getUrl()) }}" alt=""
        data-focus-left=".30" data-focus-top=".12" data-focus-right=".79" data-focus-bottom=".66" />
        @endforeach
    @endif

    <div class="container">
        <div class="info">
            @if (Route::getCurrentRoute()->uri() == '/')
            <h1 class="noUppercase">{!! isset($contentBlock[1]) ? $contentBlock[1] : '' !!}</h1>
            @else
            <h2 class="noUppercase">{!! isset($contentBlock[1]) ? $contentBlock[1] : '' !!}</h2>
            @endif
        </div>
    </div>

    <div class="heading search">
        <div class="container">
            <form id="searchForm" action="<?php echo url('search'); ?>" method="GET">
                <div class="ui grid">
                    <div class="six wide computer sixteen wide mobile column">
                        <div class="ui floating city dropdown link normal fluid inverted right labeled icon button">
                            <input type="hidden" name="regio">

                            <span class="text">Selecteer een stad</span>
                            <i class="dropdown icon"></i>

                            <div class="menu">
                                @foreach ($cityArray as $city)
                                <a href="{{ url('search?regio='.$city->slug) }}" class="item" data-value="{{ $city->slug }}">{{ $city->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="six wide computer sixteen wide mobile column">
                        <div id="sliderSearch" class="ui company search" data-type="{{ isset($cinema) ? 'cinema' : 'company' }}">
                            <div class="ui black inverted fluid icon input">
                                <input type="text" class="prompt" name="q" placeholder="Welk {{ isset($cinema) ? 'bioscoop' : 'restaurant' }} zoekt u?">
                                <i class="circular search link icon"></i>
                            </div>
                            <div class="results"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $i = 0; ?>

@inject('discountHelper', 'App\Helpers\DiscountHelper')
@foreach ($companies as $data)
    <?php $media = $data->getMedia('default'); ?>
    <div class="company hidden"
        data-kitchen="{{ is_array(json_decode($data->kitchens)) ? str_slug(json_decode($data->kitchens)[0]) : '' }}" 
        data-url="{{ url('restaurant/'.$data->slug) }}" 
        data-name="{{ $data->name }}" 
        data-address="{{ $data->address }}" 
        data-city="{{ $data->city }}" 
        data-zipcode="{{ $data->zipcode }}">
        <div class="image">
            <a  href="{{ url('restaurant/'.$data->slug) }}" title="{{ $data->name }}" class="computerImage">
                @if (isset($media[0]))
                    <img src="{{ url('public'.$media[0]->getUrl('450pic')) }}" class="ui image" alt="{{ $data->name }}" />
                @else
                    <img src="{{ url('public/images/placeholdimage.png') }}" class="ui image" alt="{{ $data->name }}" />
                @endif
            </a>

            <!-- Mobile -->
            <div class="mobileImage">
                <a href="{{ url('restaurant/'.$data->slug) }}" title="{{ $data->name }}">
                    @if(isset($media[0]))
                    <img src="{{ url('public'.$media[0]->getUrl('mobileThumb')) }}" />
                    @else
                    <img src="{{ url('public/images/placeholdimage.png') }}" />
                    @endif
                </a> 
                
                {!! $discountHelper->replaceKeys(
                        $data, 
                        $data->days, 
                        (isset($contentBlock[44]) ? $contentBlock[44] : ''),
                        'ribbon-wrapper thumb-discount-label'
                    ) 
                !!}
            </div>
            
            <div class="mobileInfo">
                <div class="right">
                    <a href="{{ url('restaurant/'.$data->slug) }}" title="{{ $data->name }}">
                        <h2>{{ $data->name }}</h2>
                    </a>

                    <a href="{{ url('search?q='.$data->city) }}">
                        <span><i class="marker icon"></i> {{ $data->city }}&nbsp;</span>
                    </a>
                </div>
            </div>
            <!-- Mobile -->
        </div>

        <div class="text">
            <a href="{{ url('restaurant/'.$data->slug) }}" title="{{ $data->name }}"><h2>{{ $data->name }}</h2></a>

            <p>{{ str_limit(strip_tags($data->menu), (isset($limitChar) ? $limitChar : 410)) }}</p><br><br>

            <div class="ui labels container grid">
                <div class="seven columns row">
                    <?php $daystoArray = json_decode($data->days); ?>
                    @foreach (Config::get('preferences.days') as $key => $day)
                    <div class="column">
                        <button class="ui {{ is_array($daystoArray) && in_array($key, $daystoArray) ? 'green' : '' }} fluid label"
                           data-open="modal" 
                           data-company="{{ $data->id }}"
                           data-title="{{ $data->name}}">
                           {{ substr($day, 0, 2) }} 
                        </button>
                    </div>
                    @endforeach
                </div>
            </div><br>
        </div>
        <div class="clear"></div>
    </div>
@endforeach
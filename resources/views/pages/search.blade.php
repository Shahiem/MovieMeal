@extends('template.theme')

{{--*/ $pageTitle = 'Zoeken' /*--}}

@section('content')
<script type="text/javascript">
    var searchPage = 1;
</script>

<div id="companies" class="content">
    <div class=" section">
        <div class="companies home">
            @if (count($companies) >= 1)
                @include('company-list')

                @if($countCompanies >= 16)
                    <div id="limitSelect" class="ui basic segment">
                        <div class="ui normal floating icon selection dropdown">
                            <i class="dropdown right floated icon"></i>
                            <div class="text">{{ $limit }} resultaten weergeven</div>
                         
                            <div class="menu">
                                <a class="item" href="{{ url('search?'.http_build_query(array_add($queryString, 'limit', '15'))) }}">15</a>
                                <a class="item" href="{{ url('search?'.http_build_query(array_add($queryString, 'limit', '30'))) }}">30</a>
                                <a class="item" href="{{ url('search?'.http_build_query(array_add($queryString, 'limit', '45'))) }}">45</a>
                            </div>
                        </div>
                    </div>
                @endif
                {!! $companies->appends($paginationQueryString)->render() !!}
            @else
                Er zijn geen restaurants gevonden met uw selectiecreteria.
            @endif
        </div>
    </div>

</div>
<div class="clear"></div>
@stop
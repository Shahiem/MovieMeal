@extends('template.theme')

{{--*/ $pageTitle = $company->name /*--}}

@section('slider')

<div class="woodBackground {{ $company->company_html == null ? 'noWidget' : '' }}">
	<div class="container">
		<div class="ui grid">
			<div class="sixteen wide mobile eight wide tablet ten wide computer column">
			@include('pages/restaurant/photos')
			</div>

			@if ($company->company_html != null)
			<div class="sixteen wide mobile eight wide tablet six wide computer column">
			 {!! $company->company_html !!}
			</div>
			@endif
		</div>
	</div>
</div>
@stop

@section('content')

	@if ($iframe == 0)
	<div class="container">
		<div id="restaurantPage">
			<div id="menuRestaurant">
				 @include('pages/restaurant/menu')
			</div>
			
		    <div class="ui breadcrumb">
				@include('pages/restaurant/breadcrumb')
			</div><br><br>

			<div class="ui vertically grid info container">
				<div class="fourteen wide computer sixteen wide mobile column">
					<div class="ui">
						<div id="restaurantMenu" class="ui bottom active attached tab" data-tab="second">
							@include('pages/restaurant/information/menu')
						</div>

						<div id="restaurantDetails" class="ui bottom attached tab" data-tab="thirds">
						  	@include('pages/restaurant/information/details')
						</div>

						<div id="restaurantContact" class="ui bottom attached tab" data-tab="four">
						  	@include('pages/restaurant/information/contact')
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>

	@include('pages/restaurant/maps')

	<div class="container">
  		<div id="moreMap">
  			<i id="mapArrow" class="circular white angle down big icon"></i>
  		</div>

		<div class="ui grid review">
			<div id="optionOne" class="sixteen wide mobile sixteen wide tablet thirtheen wide computer column">
				<h4>IN DE BUURT</h4>

				<div id="companies" class="recommend">
					<div class="companies home restaurant">
	                	@include('company-list', array('limitChar' => 120))
	        		</div>
	        	</div>

	        	@if (count($companies) > 5)
	        	<button id="loadMoreRecommend" class="ui fluid blue labeled icon button">
                    <i class="arrow cicle outline left down icon"></i>
                    Meer resultaten weergeven
                </button>
                @endif

			    <div class="clear"></div>
			</div>

		</div>
	</div>

	<div class="clear"></div>
	@endif
@stop
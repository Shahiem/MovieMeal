@extends('template.theme')

@inject('strHelper', 'App\Helpers\StrHelper')

{{--*/ $pageTitle = (isset($contentBlock[1]) ? strip_tags($contentBlock[1]) : '') /*--}}

@section('content')
    <div class="content">
        @if (isset($cinema))
        <div class="ui breadcrumb">
            <a href="{{ url('/') }}" class="section">Home</a>
            <i class="right chevron icon divider"></i>

            <span class="active section"><h1>Bioscopen</h1></span>
        </div>
        @endif

        <div class="ui equal width responsive grid container">
            <div class="column">
                @if (isset($cinema))
                {!! isset($contentBlock[3]) ? $contentBlock[3] : '' !!}
                @else
                {!! isset($contentBlock[2]) ? $contentBlock[2] : '' !!}
                @endif
            </div>
        </div> 
    </div>

    <div id="companies" class="content">
        <div class=" section">
            <div id="optionOne" class="companies home">
                @include('company-list')<br>
            </div>
        </div>

        <div class="ui grid container">
            <div class="left floated sixteen wide mobile ten wide computer column">
                {!! with(new \App\Presenter\Pagination($companies->appends($paginationQueryString)))->render() !!}
            </div>

            <div class="right floated sixteen wide mobile sixteen wide tablet three wide computer column">
                @if (count($countCompanies) >= 15)
                <div id="limitSelect">
                    <div class="ui normal floating icon selection fluid dropdown">
                        <i class="dropdown right floated icon"></i>
                        <div class="text">{{ $limit }} resultaten</div>
                                 
                        <div class="menu">
                            <a class="item" href="{{ url('/?'.http_build_query(array_add($queryString, 'limit', '15'))) }}">15</a>
                            <a class="item" href="{{ url('/?'.http_build_query(array_add($queryString, 'limit', '30'))) }}">30</a>
                            <a class="item" href="{{ url('/?'.http_build_query(array_add($queryString, 'limit', '45'))) }}">45</a>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <div class="clear"></div>
    </div>
@stop
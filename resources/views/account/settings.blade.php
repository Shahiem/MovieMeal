@extends('template.theme')

@inject('preference', 'App\Models\Preference')

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
	@if(session('success_email_message'))
		swal({ title: "Bedankt!", text: '{{ session('success_email_message') }}', type: "success", confirmButtonText: "OK" });
	@elseif(session('success_message'))
		swal({ title: "Bedankt!", text: '{{ session('success_message') }}', type: "success", confirmButtonText: "OK" });
	@endif

	$('#removeButton').click(function() {
  		swal({   
			title: "Weet u het zeker?",   
			text: "Weet u zeker dat u definitief uw account wil verwijderen?",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",  
			cancelButtonText: "Nee",   
			confirmButtonText: "Ja, ik weet het zeker!",   
		}, function() { 
			$('#deleteForm').submit(); 
			return true;
		});

		return false;
	});
});
</script>
@stop

@section('content')
<div class="container">
	<div class="ui breadcrumb">
		<a href="{{ url('/') }}" class="section">Home</a>
		<i class="right chevron icon divider"></i>

	    <a href="#" class="sidebar open">Menu</a>
	    <i class="right chevron icon divider"></i>

		<div class="active section">Account gegevens wijzigen</div>
	</div>
	<div class="ui divider"></div>

	<?php echo Form::open(array('id' => 'formList', 'url' => 'account', 'method' => 'post', 'class' => 'ui form')) ?>
		<input id="actionMan" type="hidden" name="action">

		<div class="fields">
			<div class="four wide field">
				<label>Aanhef</label>
				<?php echo Form::select('gender',  array(1 => 'Dhr', 2 => 'Mvr'), Sentinel::getUser()->gender, array('class' => 'ui normal fluid dropdown')); ?>
			</div>

			<div class="twelve wide field">
				<label>Naam</label>
				<?php echo Form::text('name', Sentinel::getUser()->name) ?>
			</div>
		</div>

		<div class="field">
			<label>E-mailadres</label>
			<?php echo Form::text('email', Sentinel::getUser()->email) ?>
		</div>

		<div class="field">
			<label>Telefoonnummer</label>
			<?php echo Form::text('phone', Sentinel::getUser()->phone) ?>
		</div>

		<div class="field">
			<label>Geboortedatum</label>
			<?php echo Form::text('birthday_at', '', array('class' => 'bdy-datepicker', 'data-value' => Sentinel::getUser()->birthday_at)); ?>
		</div>

		<h4 class="ui dividing header">Wachtwoord <small>(optioneel)</small></h4>

		<div class="two fields">
			<div class="field">
				<label>Wachtwoord</label>
				<?php echo Form::password('password') ?>
			</div>

			<div class="field">
				<label>Wachtwoord controle</label>
				<?php echo Form::password('password_confirmation') ?>
			</div>
		</div>

		<div class="field">
			<div class="ui checkbox">
				<?php echo Form::checkbox('newsletter', 1); ?>
				<label>Ik meld mij af voor de MovieMeal.nl nieuwsbrief</label>
			</div>
		</div>

		<button class="ui button" type="submit"><i class="pencil icon"></i> Wijzigen</button>
	<?php echo Form::close(); ?><br /><br>

	<?php echo Form::open(array('id' => 'deleteForm', 'url' => 'account/delete', 'method' => 'post')); ?>
		<button id="removeButton" class="ui red button" name="delete" value="1" type="button">
			<i class="remove icon"></i> Verwijder account
		</button><br /><br/>
	<?php echo Form::close(); ?>
</div>
@stop
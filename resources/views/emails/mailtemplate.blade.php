@extends('emails.template.template')
@inject('calendarHelper', 'App\Helpers\CalendarHelper')

@section('logo')
	@if (isset($info))
	<a href="{{ url('restaurant/'.$info['slug'].$extraParamaters) }}" target="_blank">
		@if(trim($logo) != '')
			<img align="left" 
				 alt="{{ $info['name'] }}" 
				 class="mcnImage" 
				 src="{{ url('public'.$logo) }}" 
				 style="width: 280px; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="200">
		@else 
			<img align="left" 
				 alt="{{ $info['name'] }}" 
				 class="mcnImage" 
			 	 src="{{ url('public/images/vplogo.png') }}" 
				 style="width: 280px; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="200">
		@endif
	</a>
	@else
		<a href="{{ url('/'.$extraParamaters) }}" target="_blank">
			<img align="left" 
				 alt="MovieMeal" 
				 class="mcnImage" 
			 	 src="{{ url('public/images/vplogo.png') }}" 
				 style="width: 280px; padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="200">
		</a>
	@endif
@endsection

@section('content')
	{!! $template !!}
@endsection

@section('maps')
	@if ($templateId != 'pay-invoice-company' && isset($info))
	<table border="0" cellpadding="0" cellspacing="0" class="mcnCodeBlock" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">
		<tbody class="mcnTextBlockOuter">
			<tr>
				<td class="mcnTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top">
					<a href="https://www.google.com/maps/dir//{{ $info['address'] }}+{{ $info['zipcode'] }}+{{ $info['city'] }}/">
						<img alt="Google Map van {{ $info['address'] }}" src="http://maps.googleapis.com/maps/api/staticmap?center={{ $info['address'] }}+{{ $info['zipcode'] }}+{{ $info['city'] }}&amp;zoom=13&amp;scale=false&amp;size=600x300&amp;maptype=roadmap&amp;format=png&amp;visual_refresh=true&amp;markers=size:small%7Ccolor:0x0080ff%7Clabel:1%7C{{ $info['address'] }}+{{ $info['zipcode'] }}+{{ $info['city'] }}" style="border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="600">
					</a>
				</td>
			</tr>
		</tbody>
	</table>
	@endif
@endsection

@section('buttons')
	<tr>
	    <td id="templateColumns" valign="top">
	        <!--[if gte mso 9]>
				<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
				<tr>
				<td align="center" valign="top" width="300" style="width:300px;">
				<![endif]-->

	        <table align="left" border="0" cellpadding="0" cellspacing="0" class="columnWrapper" width="300">
	            <tbody>
	                <tr>
	                    <td class="columnContainer" valign="top">
	                        <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonBlock" style="min-width:100%;" width="100%">
	                            <tbody class="mcnButtonBlockOuter">
	                                <tr>
	                                    <td align="center" class="mcnButtonBlockInner" style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top">
	                                        <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #2BAADF;" width="100%">
	                                            <tbody>
	                                                <tr>
	                                                    <td align="center" class="mcnButtonContent" style="font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-size: 16px; padding: 15px;" valign="middle">
	                                                        <a class="mcnButton" href="{{ url('auth/set/'.$authLinkEdit) }}" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;" target="_blank" title="Inloggen">Inloggen</a>
	                                                    </td>
	                                                </tr>
	                                            </tbody>
	                                        </table>
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </td>
	                </tr>
	            </tbody>
	        </table>
	        <!--[if gte mso 9]>
															                                    </td>
															                                    <td align="center" valign="top" width="300" style="width:300px;">
															                                    <![endif]-->

	    
	        <!--[if gte mso 9]>
				</td>
															                                    </tr>
															                                    </table>
															                                    <![endif]-->
	    </td>
	</tr>
@endsection

@section('footer')
	@if ($templateId != 'pay-invoice-company' && isset($info))
		<a href="{{ url('restaurant/'.$info['slug'].$extraParamaters) }}" target="_blank" title="{{ $info['name'] }}">{{ $info['name'] }}</a> 
		&nbsp;|&nbsp; 
		<a href="{{ url('restaurant/'.$info['slug'].$extraParamaters) }}" target="_blank" title="{{ $info['address'] }} {{ $info['zipcode'] }} {{ $info['city'] }}">
			{{ $info['address'] }} {{ $info['zipcode'] }} {{ $info['city'] }}
		</a> &nbsp;|&nbsp; 
		<a href="{{ url('restaurant/'.$info['slug'].$extraParamaters) }}" target="_blank" title="{{ $info['phone'] }}">
		{{ $info['phone'] }}
		</a><br><br />
	@else
	<a href="{{ url('/'.$extraParamaters) }}" target="_blank" title="MovieMeal B.V.">
		MovieMeal B.V.
	</a> 
	&nbsp;|&nbsp; 
	
	<a href="{{ url('/'.$extraParamaters) }}" target="_blank" title="Broenshofweide 11, 5709 SE Helmond">
		Broenshofweide 11, 5709 SE Helmond
	</a> &nbsp;|&nbsp; 

	<a href="{{ url('/'.$extraParamaters) }}" target="_blank" title="+31 (0)492-778613">
		+31 (0)492-778613
	</a>
	@endif
@endsection
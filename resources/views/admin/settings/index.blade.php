@extends('template.theme')

@section('scripts')
	<script>
	$('#preferences').repeater({
		btnAddClass: 'r-btnAdd',
		btnRemoveClass: 'r-btnRemove',
		groupClass: 'r-group',
		minItems: 1,
		maxItems: 0,
		startingIndex: {{ $lastKey }},
		reindexOnDelete: false,
		repeatMode: 'append',
		animation: null,
		animationSpeed: 400,
		animationEasing: 'swing',
		clearValues: false
	});
	</script>
@stop

@section('content')
<div class="content">
    @include('admin.template.breadcrumb')

   	<div class="ui top attached tabular menu">
		<a class="active item" data-tab="website">Website</a>
		<a class="item" data-tab="cronjobs">Cronjobs</a>
		<a class="item" data-tab="discount">Afbeeldingen</a>
		<a class="item" data-tab="eetnu">EetNU</a>
	</div>

	<div class="ui bottom active attached tab segment" data-tab="website">
		<?php echo Form::open(array('url' => 'admin/settings/website', 'method' => 'post', 'class' => 'ui form', 'files' => true)) ?>
		<div class="fields">
			<div class="four wide field">
				<div class="ui small card">
					<div class="image">
						<img src="{{ url('public/images/vplogo.png') }}">
					</div>

					<div class="extra">
						<div class="four wide field">
						   	<label>Logo</label>
						    <?php echo Form::file('logo'); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="si wide fields">
				<div class="field">
					<label>Contact email</label>
					<?php echo Form::text('contact_email', isset($websiteSettings['contact_email']) ? $websiteSettings['contact_email'] : ''); ?>
				</div>

				<div class="field">
					<label>Facebook pagina</label>
					<?php echo Form::text('facebook', isset($websiteSettings['facebook']) ? $websiteSettings['facebook'] : ''); ?>
				</div>
			</div>
		</div>


		<div class="field">
			<label>Voorkeuren</label>
			Gebruik een oplopende ID, ID nummers mogen niet hetzelfde zijn<br><br>
			<div id="preferences">
				<div id="preferencesTable">
					@foreach ($preferences as $key => $preference)
					<div  class="r-group two fields">
						<div class="twelve wide field">
							<label for="preferences_{{ $key }}_name" data-pattern-text="Commissie">Naam</label>
							<?php
							echo Form::text(
								'preferences['.$key.']', 
								$preference,
								array(
									'id' => 'preferences_'.$key.'_name',
									'class' => 'preferencesName',
									'data-pattern-name' => 'preferences[++]',
									'data-pattern-id' => 'preferences++_name'
								)
							); 
							?>
						</div>

						<div class="three wide field">
							<label>Opties</label>
							<div class="ui buttons">
								<button type="button" class="r-btnAdd ui icon button">
									<i class="add icon"></i>
								</button>	

								<button type="button" class="r-btnRemove ui red button icon">
									<i class="trash icon"></i>
								</button>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>

		<button class="ui tiny button" type="submit"><i class="plus icon"></i> Opslaan</button>
		<?php echo Form::close() ?>
	</div>

	<div class="ui bottom attached tab segment" data-tab="cronjobs">   	
		<?php echo Form::open(array('url' => 'admin/settings/cronjobs', 'method' => 'post', 'class' => 'ui form')) ?>

	   	<h4>Facturen</h4>

		<div class="two fields">
			<div class="field">
			   	<div class="ui slider checkbox">
					<input 
				  		type="checkbox" 
				  		name="mollie_invoice"
				  		{{ isset($cronjobSettings['mollie_invoice']) ? 'checked="checked"' : '' }}>
				  	<label>Stuur alle eigenaren een bericht wanneer hun geincasseerde factuur is mislukt. (incasso)</label>
				</div>
			</div>
		</div>

		<div class="two fields">
			<div class="field">
			   	<div class="ui slider checkbox">
					<input 
				  		type="checkbox" 
				  		name="product_invoice"
				  		{{ isset($cronjobSettings['product_invoice']) ? 'checked="checked"' : '' }}>
				  	<label>Stuur facturen naar klanten (Producten)</label>
				</div>
			</div>
		</div>

	   	<h4>Overige</h4>
		<div class="two fields">
			<div class="field">
			   	<div class="ui slider checkbox">
					<input 
				  		type="checkbox" 
				  		name="validate_payment"
				  		{{ isset($cronjobSettings['validate_payment']) ? 'checked="checked"' : '' }}>
				  	<label>Keur mollie betalingen</label>
				</div>
			</div>
		</div>

		<div class="two fields">
			<div class="field">
			   	<div class="ui slider checkbox">
					<input 
				  		type="checkbox" 
				  		name="call_list"
				  		{{ isset($cronjobSettings['call_list']) ? 'checked="checked"' : '' }}>
				  	<label>Voeg bedrijven toe aan bellijst <span class="ui red small header color">nieuw</span></label>
				</div>
			</div>
		</div>

		<br /><br />

		<button class="ui tiny button" type="submit"><i class="plus icon"></i> Opslaan</button>
		<?php echo Form::close() ?>
	</div>

	<div class="ui bottom attached tab segment" data-tab="discount">
		<?php echo Form::open(array('url' => 'admin/settings/discount', 'method' => 'post', 'class' => 'ui form', 'files' => true)) ?>
		<h4>Header</h4>

		<div class="fields">
			<div class="four wide field">
			   	<label>Afbeeldingen</label>
			   	<?php echo Form::file('header[]', array('multiple' => true, 'class' => 'multi with-preview')); ?><br /><br />
			</div>
		</div>

		<div class="ui five cards">
			@if (count($mediaItems) >= 1)
				@foreach ($mediaItems as $id => $images)
				<div class="card">
					<div class="image">
						<img src="{{ url('public/'.$images->getUrl('thumb')) }}" class="ui image">
					</div>

					<div class="extra">
						<a href="{{ url('admin/settings/delete/image/'.$id) }}">
							<i class="trash icon"></i>
						</a>
					</div>
				</div>
				@endforeach
			@endif
		</div>

		<div class="fields">
			<div class="twelve wide field">
			    <label>Breedte</label>
			    <?php echo Form::text('discount_width4', isset($discountSettings['discount_width4']) ? $discountSettings['discount_width4'] : ''); ?>
			</div>

			<div class="twelve wide field">
			    <label>Hoogte</label>
			    <?php echo Form::text('discount_height4', isset($discountSettings['discount_height4']) ? $discountSettings['discount_height4'] : ''); ?>
			</div>
		</div>

		<h4>Google Pointer - Restaurant</h4>

		<div class="fields">
			<div class="four wide field">
			   	<label>Afbeelding</label>
			    <?php echo Form::file('company_google_pointer'); ?>
			</div>
		</div>

		<h4>Google Pointer - Bioscoop</h4>

		<div class="fields">
			<div class="four wide field">
			   	<label>Afbeelding</label>
			    <?php echo Form::file('cinema_google_pointer'); ?>
			</div>
		</div><br>

		<button class="ui tiny button" type="submit"><i class="plus icon"></i> Opslaan</button>
		<?php echo Form::close() ?>
	</div>

	<div class="ui bottom attached tab segment" data-tab="eetnu">
		<?php echo Form::open(array('url' => 'admin/settings/eetnu', 'method' => 'post', 'class' => 'ui form', 'files' => true)) ?>
		<h4>Keuken</h4>
		<div class="two fields">
			<div class="field">
			   	<?php 
			   	echo Form::select(
			   		'kitchens[]', 
			   		$kitchens,
					(isset($kitchensSettings['kitchens']) && $kitchensSettings['kitchens'] != 'null' ? json_decode($kitchensSettings['kitchens']) : ''),
					array('multiple' => true, 'class' => 'multipleSelect')
			   	);
			   	?>
			</div>
		</div>

		<h4>Steden</h4>
		Welke steden zijn <strong>NIET</strong> toegestaan? Alle steden die zijn geselecteerd worden niet meegenomen.<br /><br />
		<div class="two fields">
			<div class="field">
			   	<?php 
			   	echo Form::select(
			   		'cities[]', 
			   		$cities,
					(isset($kitchensSettings['cities']) && $kitchensSettings['cities'] != 'null' ? json_decode($kitchensSettings['cities']) : ''),
					array('multiple' => true, 'class' => 'multipleSelect')
			   	);
			   	?>
			</div>
		</div>

		<button class="ui tiny button" type="submit"><i class="plus icon"></i> Opslaan</button>
		<?php echo Form::close() ?>
	</div>

	<div class="ui bottom attached tab segment" data-tab="invoices">
		Dit gedeelte is bedoeld voor het versturen van de facturen van de opstartkosten. Vul hieronder het bedrag in die de bedrijven moeten betalen voor aanmelding bij UWvoordeelpas. Dit geldt alleen voor <strong>NIEUWE</strong> bedrijven die komen uit de bellijst.<br><br>
		<?php echo Form::open(array('url' => 'admin/settings/invoices', 'method' => 'post', 'class' => 'ui form')) ?>
			<div class="field">
			   	<div class="ui slider checkbox">
				  	<input 
					  	type="checkbox" 
				  		name="services_noshow"
				  		{{ isset($invoicesSettings['services_noshow']) ? 'checked="checked"' : '' }}>
				  	<label>Geen opstartkosten</label>
				</div>
			</div>

			<div class="two fields">
			<div class="field">
				<label>Naam</label>
				<?php echo Form::text('name', isset($invoicesSettings['services_name']) ? $invoicesSettings['services_name'] : ''); ?>
			</div>

			<div class="field">
				<label>Perodiek</label>
				Eenmalig
			</div>
		</div>	

		<div class="two fields">
			<div class="field">
				<label>Prijs</label>
				<?php echo Form::number('price', isset($invoicesSettings['services_price']) ? $invoicesSettings['services_price'] : ''); ?>
			</div>	

			<div class="field">
				<label>BTW</label>
				<?php echo Form::number('tax', isset($invoicesSettings['services_tax']) ? $invoicesSettings['services_tax'] : ''); ?>
			</div>	
		</div>	

		<button class="ui tiny button" type="submit"><i class="plus icon"></i> Opslaan</button>
		<?php echo Form::close() ?>
	</div>
</div>
@stop
@extends('template.theme')

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
		    closeBrowser();  
		});
	</script>
@stop

@section('content')
<div class="content">
 	@include('admin.template.breadcrumb')
 	
	<?php echo Form::open(array('url' => 'admin/'.$slugController.'/create', 'method' => 'post', 'class' => 'ui edit-changes form', 'files' => TRUE)) ?>
	    <div class="left section">
			<div class="two fields">
				<div class="field">
				    <label>Bedrijf *</label>
				    <?php echo Form::select('company', array_add($companies, ' ', 'Kies een bedrijf'), ' ', array('id' => 'barcodesCompanies', 'class' => 'ui normal search dropdown'));  ?>
				</div>	

			    <div class="field">
			    	<label>Code</label>
			    	<?php echo Form::text('code'); ?>
			    </div>	
			</div>	

			<div class="two fields">
			    <div class="field">
			    	<label>Dienst *</label>

					<div id="barcodesServices" class="ui normal fluid dropdown product-services selection" tabindex="0">
						<?php echo Form::hidden('service_id'); ?>

						<i class="dropdown icon"></i>
						<div class="default text">Kies een dienst</div>

						<div class="menu" tabindex="-1">
							
						</div>
					</div>
			    </div>	

			    <div class="field">
			    	<label>Verloopt op</label>
			    	<?php echo Form::text('expire_date', '', array('class' => 'datepicker')); ?>
			    </div>	
			</div>	

			<div class="field">
			    <label>PDF bestanden *</label>
			    <?php echo Form::file('pdf[]', array('multiple' => true)); ?>
    		</div>

			<button class="ui button" type="submit"><i class="plus icon"></i> Aanmaken</button>
		</div>

		<div class="right section" style="padding-left: 20px;">
			<div class="field">
				<label>Barcode inschakelen</label>
				<div class="ui toggle checkbox">
					<?php echo Form::checkbox('is_active', 1, 1); ?>
					<label>Actief</label>
				</div>
			</div>
		</div>
	<?php echo Form::close(); ?>
</div>
<div class="clear"></div>
@stop
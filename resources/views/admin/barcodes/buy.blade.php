<?php echo Form::open(array('url' => 'admin/barcodes/buy', 'method' => 'post', 'class' => 'ui buy form')) ?>
	<?php echo Form::hidden('service_id', $serviceId); ?>
	<?php echo Form::hidden('service_name', $serviceName); ?>
	<?php echo Form::hidden('company_id', $companyId); ?>
	<?php echo Form::hidden('company_name', $companyName); ?>

	<div class="field">
		<label>Aantal</label>
		<?php echo Form::number('amount_tickets', 1, array('autocomplete' => 'off', 'min' => 1)); ?>
	</div>	

	<div class="three fields">
		<div class="field">
			<label>Prijs per kaart</label>
			&euro;{{ $price }}
		</div>	

		<div class="field">
			<label>Totaal excl btw</label>
			&euro;<span data-price="{{ $price }}" data-tax="1.{{ $tax < 10 ? '0'.$tax : $tax }}" data-identifer="total">{{ $price }}</span>
		</div>	

		<div class="field">
			<label>Totaal incl btw</label>
			&euro;<span data-identifer="totalinctax">{{ $price }}</span>
		</div>	
	</div><br>

	<button class="ui red fluid button" type="submit">Kopen</button>
<?php echo Form::close(); ?>
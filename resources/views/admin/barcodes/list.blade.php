@foreach($data as $fetch)
    <?php
    $expireDate = $fetch->expire_date != NULL ? date('d-m-Y', strtotime($fetch->expire_date)): date('d-m-Y', strtotime('+1 year', strtotime($fetch->created_at)));
    ?>

    <tr>
        <td>
            <div class="ui child checkbox">
            	<input type="checkbox" name="id[]" value="{{ $fetch->id }}">
            	<label></label>
            </div>
        </td>
        <td>{{ $fetch->code == NULL ? 'Geen' : $fetch->code }}</td>
        <td>{{ $fetch->companyName }}</td>
        <td>{{ $fetch->serviceName }}</td>
        <td>{{ $fetch->amount }}</td>
        <td>{{ $fetch->serviceName }}</td>
        <td>{{ $expireDate }} </td>
        <td>
            <a href="{{ url('admin/'.$slugController.'/update/'.$fetch->id) }}" class="ui icon button">
                <i class="pencil icon"></i> Bewerk
            </a>

            <buton data-open="paymentPopup" data-id="{{ $fetch->id }}" data-name="{{ $fetch->companyName }}" class="ui red icon button">
                <i class="ticket icon"></i> Koop E-ticket
            </button>
        </td>
    </tr>
@endforeach
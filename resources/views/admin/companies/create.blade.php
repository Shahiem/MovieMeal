@extends('template.theme')

@inject('preference', 'App\Models\Preference')

@section('scripts')
	@include('admin.template.editor')

	<script type="text/javascript">
		$(document).ready(function() {
		    closeBrowser();  
		});
	</script>
@stop

@section('content')
<div class="content">
	@include('admin.template.breadcrumb')

	<?php echo Form::open(array('url' => 'admin/'.$slugController.'/create', 'method' => 'post', 'class' => 'ui form', 'files' => TRUE)) ?>
		<div class="left section">
			<div class="field">
				<div class="ui checkbox">
			   	 	<?php echo Form::checkbox('no_show', 1) ?>
			   	 	<label>Niet tonen op homepagina en zoeken</label>
				</div>
			</div>
			
			<div class="field">
			    <label>Naam</label>
			    <?php echo Form::text('name') ?>
			</div>
			
			<div class="field">
				<label>Type</label>
				<?php
				echo Form::select('company_type', array(
					0 => 'Restaurant',
					1 => 'Bioscoop'
				), '', array('class' => 'ui normal search dropdown'));
				?>
			</div>

			<div class="ui top attached tabular menu">
			  	<a class="item" data-tab="second">Menu</a>
			  	<a class="item" data-tab="third">Widget</a>
			</div>

			<div class="ui bottom attached tab segment" data-tab="second">
			  	<div class="field">
				    <label>Menu</label>
				    <?php echo Form::textarea('menu', '', array('class' => 'editor')) ?>
				</div>
			</div>
			<div class="ui bottom attached tab segment" data-tab="third">
			  	<div class="field">
				    <label>Widget</label>
				    <?php echo Form::textarea('company_html', '') ?>
				</div>
			</div>
			<div class="ui bottom attached tab segment" data-tab="four">
			  	<div class="field">
				    <label>Contact details</label>
				    <?php echo Form::textarea('contact', '', array('class' => 'editor')) ?>
				</div>
			</div><br />

			<div class="field">
				<label>Eigenaar</label>
				Kies een gebruiker uit die eigenaar is van dit bedrijf.<br /><br />
				
				<div class="field">
					<div class="ui checkbox">
					   	 <?php echo Form::checkbox('new_user', 1) ?>
					   	 <label>Een nieuwe gebruiker</label>
					</div>
				</div>

				<div class="ui horizontal divider">OF</div>

				<strong>Bestaande gebruiker</strong><br />
				<div id="companiesOwnersSearch" class="ui search">
					<div class="ui icon fluid input">
						<input class="prompt" type="text" placeholder="Typ een naam in..">
						<i class="search icon"></i>
					</div>

					<div class="results"></div>
				</div>
			</div>

			<h4>Contactgegevens</h4>
			<div class="three fields">
				<div class="field">
				    <label>Adres</label>
				    <?php echo Form::text('address') ?>
				</div>

				<div class="field">
				    <label>Postcode</label>
				    <?php echo Form::text('zipcode') ?>
				</div>
				<div class="field">
					<label>Woonplaats</label>
					<?php echo Form::text('city', '') ?>
				</div>
			</div>

			<div class="three fields">
				<div class="field">
					<label>Website</label>
					<?php echo Form::text('website', '') ?>
				</div>
			</div>

			<h4>Administratie</h4>
			<div class="three fields">
				<div class="field">
				    <label>Contactpersoon</label>
				    <?php echo Form::text('contact_name') ?>
				</div>

				<div class="field">
				    <label>Telefoon (contactpersoon)</label>
				    <?php echo Form::text('phone') ?>
				</div>

				<div class="field">
				    <label>Emailadres (contactpersoon)</label>
				    <?php echo Form::text('email') ?>
				</div>
			</div>

			<div class="three fields">
				<div class="field">
				    <label>Functie</label>
				    <?php echo Form::text('contact_role') ?>
				</div>

				<div class="field">
				    <label>KVK</label>
				    <?php echo Form::text('kvk') ?>
				</div>

				<div class="field">
				    <label>BTW</label>
				    <?php echo Form::text('btw') ?>
				</div>
			</div>

			<div class="three fields">
				<div class="field">
				    <label>IBAN Nummer</label>
				    <?php echo Form::text('financial_iban') ?>
				</div>

				<div class="field">
				    <label>IBAN tnv</label>
				    <?php echo Form::text('financial_iban_tnv') ?>
				</div>

				<div class="field">
				    <label>E-mailadres (administratie)</label>
				    <?php echo Form::text('financial_email') ?>
				</div>
			</div>

			<h4 class="ui dividing header">Voorkeuren</h4>

			<div class="two fields">
				<div class="field">
					<label>Regio</label>
					<?php
					$regio = array();
					$regio[''] = 'Regio';

					foreach($preference->where('category_id', 9)->get() as $data) {
						$regio[$data->id] = $data->name;
					}

					echo Form::select('regio[]', $regio, null, array('class' => 'ui normal search dropdown'));
					?>
				</div>
				
				<div class="field">
						<label>Moviemeal dagen</label>
					<?php 
					$discountDaysArray = array(
						'multiple' => true, 
						'id' => 'discountDays', 
						'class' => 'ui normal search dropdown'
					);

					echo Form::select('days[]', Config::get('preferences.days'), '', $discountDaysArray);
					?>
				</div>
			</div>

			<button class="ui tiny button" type="submit"><i class="plus icon"></i> Aanmaken</button>
		</div>

		<div class="right section" style="padding-left: 20px;">
			@if($userAdmin)
			<div class="field">
				<label>PDF uploaden</label>
				<?php echo Form::file('pdf[]', array('multiple' => true)); ?>
			</div>
			@endif

			<div class="field">
				<label>Afbeeldingen (maximaal 6)</label>
				Middels u muis kunt u ze in de gewenste volgorde slepen.<br /><br />
				<?php echo Form::file('images[]', array('multiple' => true, 'class' => 'multi with-preview')); ?>
			</div>
		</div>
	<?php echo Form::close(); ?>
</div>
<div class="clear"></div>
@stop

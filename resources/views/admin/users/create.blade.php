@extends('template.theme')

@inject('preference', 'App\Models\Preference')

@section('content')
<div class="content">
    @include('admin.template.breadcrumb')

	<?php echo Form::open(array('url' => 'admin/'.$slugController.'/create', 'method' => 'post', 'class' => 'ui edit-changes form')) ?>
		<div class="field">
			<label>Rol</label>
			<?php 
			echo Form::select(
				'role',
				array(
					1 => 'Lid', 
					2 => 'Bedrijf',
					3 => 'Admin',
					4 => 'Bediening',
					5 => 'Callcenter'
				), 
				1, 
				array('class' => 'multipleSelect')
			); 
			?>
		</div>

		<div class="fields">
			<div class="four wide field">
			   	<label>Aanhef</label>
				<?php echo Form::select('gender',  array(1 => 'Dhr', 2 => 'Mvr'), '', array('class' => 'multipleSelect')); ?>
			</div>

			<div class="twelve wide field">
			    <label>Naam</label>
			    <?php echo Form::text('name') ?>
			</div>
		</div>

		<div class="field">
			<label>E-mailadres</label>
			<?php echo Form::text('email') ?>
		</div>

		<div class="field">
			<label>Telefoonnummer</label>
			<?php echo Form::text('phone') ?>
		</div>

		<div class="field">
			<label>Geboortedatum</label>
			<?php echo Form::text('birthday_at', '', array('class' => 'ajax-datepicker')); ?>
		</div>

		<h4 class="ui dividing header">Wachtwoord</h4>

		<div class="field">
		    <label>Wachtwoord</label>
		    <?php echo Form::password('password') ?>
		</div>

		<div class="field">
		  <label>Wachtwoord controle</label>
		  <?php echo Form::password('password_confirmation') ?>
		</div>

		 <button class="ui tiny button" type="submit"><i class="plus icon"></i> Aanmaken</button>
	<?php echo Form::close(); ?>
</div>
<div class="clear"></div>
@stop
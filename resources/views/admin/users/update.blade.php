@extends('template.theme')

@inject('cityPref', 'App\Models\Preference')

@section('content')
<div class="content">

    @if (isset($data))
    @include('admin.template.breadcrumb')
	<?php echo Form::open(array('url' => 'admin/'.$slugController.'/update/'.$data->id, 'method' => 'post', 'class' => 'ui edit-changes form')) ?>
		<div class="field">
			<label>Rol</label>
			<?php 
			echo Form::select(
				'role',
				array(
					1 => 'Lid', 
					2 => 'Bedrijf',
					3 => 'Admin',
					4 => 'Bediening',
					5 => 'Callcenter'				
				), 
				$data->default_role_id, 
				array('class' => 'multipleSelect')
			); 
			?>
		</div>
		<div class="fields">
			<div class="four wide field">
			   	<label>Aanhef</label>
				<?php echo Form::select('gender',  array(1 => 'Dhr', 2 => 'Mvr'), $data->gender, array('class' => 'multipleSelect')); ?>
			</div>

			<div class="twelve wide field">
			    <label>Naam</label>
			    <?php echo Form::text('name', $data->name) ?>
			</div>
		</div>

		<div class="field">
			<label>E-mailadres</label>
			<?php echo Form::text('email', $data->email) ?>
		</div>

		<div class="field">
			<label>Telefoonnummer</label>
			<?php echo Form::text('phone', $data->phone) ?>
		</div>
		
		<div class="field">
			<label>Geboortedatum</label>
			<?php echo Form::text('birthday_at', '', array('class' => 'bdy-datepicker', 'data-value' => $data->birthday_at)); ?>
		</div>

		<h4 class="ui dividing header">Wachtwoord <small>(optioneel)</small></h4>

		<div class="field">
		    <label>Wachtwoord</label>
		    <?php echo Form::password('password') ?>
		</div>

		<div class="field">
		  <label>Wachtwoord controle</label>
		  <?php echo Form::password('password_confirmation') ?>
		</div>

		 <button class="ui tiny button" type="submit"><i class="pencil icon"></i> Wijzigen</button>
		 <a href="{{ url('admin/ban/create/'.$data->id) }}" class="ui tiny red button" type="submit"><i class="ban icon"></i> Verbannen</a>
	<?php echo Form::close(); ?>
	@endif
</div>
<div class="clear"></div>
@stop
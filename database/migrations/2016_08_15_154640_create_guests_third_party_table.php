<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestsThirdPartyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests_third_party', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reservation_number', 500)->nullable();
            $table->string('network_status');
            $table->string('reservation_status');
            $table->string('name', 500);
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('persons');
            $table->mediumText('comment');
            $table->string('network');
            $table->integer('restaurant_id')->nullable();
            $table->string('restaurant_name')->nullable();
            $table->string('restaurant_address')->nullable();
            $table->string('restaurant_zipcode')->nullable();
            $table->timestamp('reservation_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('guests_third_party');
    }
}

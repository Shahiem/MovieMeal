<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 500);
            $table->string('name', 500);
            $table->integer('category_id')->nullable();
            $table->timestamps();
        });

        Schema::table('faq_questions', function (Blueprint $table) {
            $table->integer('subcategory')->nullable();       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('faq_categories');

        Schema::table('faq_questions', function (Blueprint $table) {
            $table->dropColumn('subcategory');       
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrackingLinkToAffiliates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('affiliates', function(Blueprint $table) {
            $table->string('tracking_link', 500)->after('link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('affiliates', function(Blueprint $table) {
            $table->dropColumn('tracking_link');
        });
    }
}

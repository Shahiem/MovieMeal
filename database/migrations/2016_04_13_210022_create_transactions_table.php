<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('external_id')->nullable();
            $table->integer('program_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->decimal('amount', 8, 4)->unsigned();
            $table->string('status', 50);
            $table->string('ip', 64);
            $table->dateTime('processed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}

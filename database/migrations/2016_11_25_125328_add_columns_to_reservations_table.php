<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_reservations', function (Blueprint $table) {
            $table->integer('extra_reservations');
            $table->time('closed_at')->change();
            $table->renameColumn('closed_at', 'closed_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_reservations', function (Blueprint $table) {
            $table->dropColumn('extra_reservations');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table
            	->increments('id')
            ;

            $table
                ->integer('send_mail')
            ;
            
            $table
                ->integer('send_reminder')
            ;
            
            $table
                ->longText('comment')
                ->nullable()
            ;
            
            $table
                ->text('status')
                ->nullable()
            ;

            $table
                ->text('email')
            ;
            
            
            $table
                ->longText('place')
                ->nullable()
            ;
            
            $table
                ->timestamp('appointment_at')
            ;
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('appointments');
    }
}

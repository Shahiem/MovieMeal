<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToIncassoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incassos', function (Blueprint $table) {
            $table
                ->string('type')
                ->after('no_of_invoices')
            ;      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incassos', function (Blueprint $table) {
            $table
                ->dropColumn('type')
            ;
        });
    }
}

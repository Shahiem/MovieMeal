<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCallcenterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies_callcenter', function (Blueprint $table) {
            $table->longText('preferences')->nullable();
            $table->longText('price')->nullable();
            $table->longText('kitchens')->nullable();
            $table->longText('allergies')->nullable();
            $table->longText('facilities')->nullable();
            $table->longText('kids')->nullable();
            $table->longText('person')->nullable();
            $table->longText('sustainability')->nullable();
            $table->longText('discount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies_callcenter', function (Blueprint $table) {
            $table->dropColumn('preferences');    
        });
    }
}

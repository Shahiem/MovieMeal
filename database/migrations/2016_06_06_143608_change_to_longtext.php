<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeToLongtext extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incassos', function (Blueprint $table) {
            $table->decimal('amount', 10,2)->after('no_of_invoices');

            $table->string('xml')->change();
            $table->longText('xml')->change();
        });

        Schema::table('affiliates', function (Blueprint $table) {
            $table->string('compensations')->change();
            $table->longText('compensations')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('name');
            $table->integer('user_id')->nullable();
            $table->text('description')->nullable();
            $table->longText('about_us')->nullable();
            $table->longText('menu')->nullable();
            $table->longText('details')->nullable();
            $table->longText('contact')->nullable();
            
            $table->string('website')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('city')->nullable();

            $table->string('contact_name')->nullable();
            $table->string('contact_phone')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_role')->nullable();

            $table->string('financial_iban')->nullable();
            $table->string('financial_iban_tnv')->nullable();
            $table->string('financial_email')->nullable();

            $table->longText('preferences')->nullable();
            $table->longText('price')->nullable();
            $table->longText('kitchens')->nullable();
            $table->longText('allergies')->nullable();
            $table->longText('facilities')->nullable();
            $table->longText('kids')->nullable();
            $table->longText('person')->nullable();
            $table->longText('sustainability')->nullable();
            $table->longText('discount')->nullable();

            $table->string('kvk')->nullable();
            $table->string('btw')->nullable();

            $table->integer('no_show');
            $table->text('regio');
            $table->text('days');

            $table->string('pdf_activate_code')->nullable();
            $table->string('pdf_name')->nullable();
            $table->integer('pdf_active')->unsigned();
            $table->integer('from_company_id')->unsigned();

            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table
                ->decimal('total_price', 10,2)
            ;

            if (Schema::hasColumn('invoices', 'lastsend_at')) {
                $table
                    ->renameColumn('lastsend_at', 'next_invoice_at')
                ;
            }
            
            if (Schema::hasColumn('invoices', 'next_invoice_at')) {
                $table
                    ->date('next_invoice_at')
                    ->nullable()
                    ->change()
                ;
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table
                ->dropColumn('total_price')
            ;
        });
    }
}

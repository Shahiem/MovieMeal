<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table
                ->text('invoice_number')
                ->after('id')
            ;

            $table->date('start_date');

            $table
                ->string('type')
            ;

            $table->integer('period');

            $table->longText('products');

            $table
                ->timestamps()
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn(
                array(
                    'period',
                    'type',
                    'start_date',
                    'end_date',
                    'products',
                    'created_at',
                    'updated_at'
                )
            );
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraColumnsToCallcenterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies_callcenter', function (Blueprint $table) {
            $table->timestamp('callback_at')->before('called_at');
            $table->integer('caller_id')->before('callback_at');
        });
    }

    /**
     * Reverse the migrations.
     *                              
     * @return void
     */
    public function down()
    {
        Schema::table('companies_callcenter', function (Blueprint $table) {
            $table->dropColumn('callback_at');
            $table->dropColumn('caller_id');
        });
    }
}

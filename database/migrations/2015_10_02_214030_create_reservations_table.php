<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->time('time');
            $table->integer('user_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->integer('persons');
            $table->integer('no_show');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->text('preferences');
            $table->text('allergies');
            $table->text('comment');
            $table->decimal('saldo', 5, 2)->default(0);
            $table->integer('newsletter_company');
            $table->integer('is_cancelled');
            $table->timestamps();

            $table->index(['company_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}

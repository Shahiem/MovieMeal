<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTrashAble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn([
                'kitchens', 
                'discount_comment', 
                'price_per_guest', 
                'allergies', 
                'person', 
                'facilities', 
                'preferences', 
                'price', 
                'sustainability', 
                'discount', 
                'days'
            ]);
        });

        Schema::dropIfExists('reservations');
        Schema::dropIfExists('company_reservations');
        Schema::dropIfExists('incassos');
        Schema::dropIfExists('guests_wifi');
        Schema::dropIfExists('guests');
        Schema::dropIfExists('guests_third_party');
        Schema::dropIfExists('favorite_affiliates');
        Schema::dropIfExists('favorite_companies');
        Schema::dropIfExists('users_transactions');
        Schema::dropIfExists('categories');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            //
        });
    }
}

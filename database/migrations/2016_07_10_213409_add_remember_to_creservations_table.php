<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRememberToCreservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_reservations', function (Blueprint $table) {
            $table
                ->integer('reminder_before_date')
                ->default(0)
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_reservations', function (Blueprint $table) {
            $table
                ->dropColumn('reminder_before_date')
            ;
        });
    }
}

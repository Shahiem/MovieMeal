<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsmanualColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_reservations', function (Blueprint $table) {
            $table->integer('is_manual')->after('update_before_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_reservations', function (Blueprint $table) {
            $table->dropColumn('is_manual');
        });
    }
}

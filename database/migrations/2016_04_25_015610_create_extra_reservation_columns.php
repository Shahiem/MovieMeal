<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraReservationColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations', function(Blueprint $table) {
            $table->integer('user_is_paid_back')->after('is_cancelled');
            $table->integer('restaurant_is_paid')->after('user_is_paid_back');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('reservations', function(Blueprint $table) {
            $table->dropColumn('user_is_paid_back');
            $table->dropColumn('restaurant_is_paid');
        });
    }
}

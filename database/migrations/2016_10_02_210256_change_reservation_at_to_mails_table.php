<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeReservationAtToMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guests_third_party', function (Blueprint $table) {
            $table->dateTime('reservation_date')->nullable();
            $table->string('mail_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guests_third_party', function (Blueprint $table) {
            $table->dropColumn('reservation_date');
            $table->dropColumn('mail_id');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table
                ->date('end_date')
                ->after('start_date')
            ;
        });

        Schema::table('company_services', function (Blueprint $table) {
            $table
                ->date('start_date')
                ->after('period')
            ;

            $table
                ->date('end_date')
                ->after('start_date')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table
                ->dropColumn('end_date')
            ;
        });

        Schema::table('company_services', function (Blueprint $table) {
            $table
                ->dropColumn('start_date')
            ;

            $table
                ->dropColumn('end_date')
            ;
        });
    }
}

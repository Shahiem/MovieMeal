<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->time('start_time');
            $table->time('end_time');
            $table->integer('per_time');
            $table->longText('available_persons');
            $table->longText('available_deals');
            $table->integer('company_id');
            $table->integer('is_locked');
            $table->integer('max_persons');
            $table->integer('closed_before_time');
            
            $table->longText('locked_times');
            $table->timestamps();

            $table->index(['company_id', 'date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('company_reservations');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesExtraColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->date('date')->after('id');
            $table->integer('week')->after('date');
            $table->decimal('total_saldo', 10,2);
            $table->integer('total_persons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function(Blueprint $table) {
            $table->dropColumn('week');
            $table->dropColumn('date');
            $table->dropColumn('total_saldo');
            $table->dropColumn('total_persons');
        });
    }
}

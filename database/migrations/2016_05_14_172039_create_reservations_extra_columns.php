<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsExtraColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_reservations', function (Blueprint $table) {
            $table->integer('cancel_before_time')->after('closed_before_time');
            $table->integer('update_before_time')->after('cancel_before_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_reservations', function(Blueprint $table) {
            $table->dropColumn('cancel_before_time');
            $table->dropColumn('update_before_time');
        });
    }

}

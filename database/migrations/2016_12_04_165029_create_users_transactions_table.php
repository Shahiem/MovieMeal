<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id')->nullable();
            $table->text('description');
            $table->text('amount_description');
            $table->decimal('amount', 5, 2);
            $table->decimal('old_saldo', 5, 2);
            $table->decimal('new_saldo', 5, 2);
            $table->timestamps();
        });

        Schema::rename('transactions', 'affiliates_transactions');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_transactions');
    }
}

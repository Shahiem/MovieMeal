<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CleanupAffiliatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliates', function (Blueprint $table) {
            if (Schema::hasColumn('affiliates', 'percentage')) {
                $table
                    ->dropColumn('percentage')
                ;
            }

            if (Schema::hasColumn('affiliates', 'cash_amount')) {
                $table
                    ->dropColumn('cash_amount')
                ;
            }

            if (Schema::hasColumn('affiliates', 'terms')) {
                $table
                    ->dropColumn('terms')
                ;
            }

            if (Schema::hasColumn('affiliates', 'category_id')) {
                $table
                    ->dropColumn('category_id')
                ;
            }
            
            if (Schema::hasColumn('affiliates', 'subcategory_id')) {
                $table
                    ->dropColumn('subcategory_id')
                ;
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

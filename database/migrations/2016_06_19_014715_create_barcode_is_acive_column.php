<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarcodeIsAciveColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('barcodes_users', function (Blueprint $table) {
            $table->integer('is_active')->after('id');
            $table->integer('company_id')->nullable()->after('user_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barcodes_users', function (Blueprint $table) {
            $table->dropColumn('is_active');
            $table->dropColumn('company_id');
        });
    }
}
